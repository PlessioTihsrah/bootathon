function perform(operation: string) {
  let inputOne: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("numberOne")
  );
  let inputTwo: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("numberTwo")
  );
  let result: HTMLSpanElement = <HTMLSpanElement>(
    document.getElementById("result")
  );
  let err: string;
  const valueOne = inputOne.value;
  const valueTwo = inputTwo.value;
  if (
    isNaN(+valueOne) ||
    isNaN(+valueTwo) ||
    valueOne.length === 0 ||
    valueTwo.length === 0
  ) {
    err = "Invalid Numbers";
    operation = "error";
  }
  switch (operation) {
    case "add":
      result.innerText = (+valueOne + +valueTwo).toString();
      break;
    case "subtract":
      result.innerText = (+valueOne - +valueTwo).toString();
      break;
    case "multiply":
      result.innerText = (+valueOne * +valueTwo).toString();
      break;
    case "divide":
      result.innerText = (+valueOne / +valueTwo).toString();
      break;
    default:
      result.innerText = err;
  }
}
