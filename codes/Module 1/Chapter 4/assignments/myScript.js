function perform(operation) {
    let inputOne = (document.getElementById("numberOne"));
    let inputTwo = (document.getElementById("numberTwo"));
    let result = (document.getElementById("result"));
    let err;
    const valueOne = inputOne.value;
    const valueTwo = inputTwo.value;
    if (isNaN(+valueOne) ||
        isNaN(+valueTwo) ||
        valueOne.length === 0 ||
        valueTwo.length === 0) {
        err = "Invalid Numbers";
        operation = "error";
    }
    switch (operation) {
        case "add":
            result.innerText = (+valueOne + +valueTwo).toString();
            break;
        case "subtract":
            result.innerText = (+valueOne - +valueTwo).toString();
            break;
        case "multiply":
            result.innerText = (+valueOne * +valueTwo).toString();
            break;
        case "divide":
            result.innerText = (+valueOne / +valueTwo).toString();
            break;
        default:
            result.innerText = err;
    }
}
//# sourceMappingURL=myScript.js.map