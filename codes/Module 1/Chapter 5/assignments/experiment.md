# Nature of Flow

## Aim

The object of this experiment is to study gas liquid absorption in an absorption column:-

1. To visually observe laminar and turbulent flow patterns.
2. To determine the critical Reynolds Number for transition from laminar to turbulent flow.
3. To determine the friction-factor and to verify the relationship between friction factor (f) and Reynolds Number (N<sub>Re</sub>) .

---

## Theory

Pipe flow problems occur in the design of many types of engineering projects. A few of these include refineries (liquid flow), gasoline plants (gas or vapor flow), chemical plants etc. Fluid flow is an integral part of any chemical industry and understanding the phenomena that dictate the motion of any fluid through popes is of utmost importance to any chemical engineer. There is always need of develop an efficient system. To reduce wastage of energy through pipe flow, it would be beneficial to first study the various flow regimes of fluids on a small scale.

Here are some formulae which we are using in this experiment:

N<sub>Re</sub> = ρvD/μ

N<sub>Re</sub> is a dimensionless number called Reynolds number. ρ is density of fluid, V is velocity, D is internal diameter of pipe in which fluid flows and μ is the viscosity of fluid. For calculation purpose all the parameters are taken in SI units.

f= (ΔPg<sub>c</sub>D)/2Lρv<sup>2</sup> =gΔhD/2Lv<sup>2</sup>

f is friction-factor. This formula relates friction-factor with various parameters. Some of them we find experimentally and others are constant. ΔP is pressure drop, D is inner diameter, L is length of pipe and V is velocity. gc is equal to one here because we are using SI units in given expression. Δh is the height difference of two water tanks.

There are some correlations between Reynolds number and friction-factor. For laminar flow it’s given by:

f=16/ N<sub>Re</sub>

For turbulent two correlations exist:

f=0.046NRe-0.2 (50000 < N<sub>Re</sub> < 10<sub>6</sub> )<br>
f=0.0014 + (0.125/ NRe0.32) (3000 < N<sub>Re</sub> < 3x10<sub>6</sub>)

![Diagram](https://ce-iitb.vlabs.ac.in/exp5/images/nof1.PNG)

## Procedure

1. Allow the water to fill the equipment and adjust the flow rate to the lowest possible value.
2. Adjust the flow rate of the dye solution so that its flow rate is the same as the velocity of water.
3. Measure the volumetric flow rate using a measuring vessel and a stop watch.
4. Increase the flow rate of water in small increments and repeat the above steps.
5. From the highest flow rate decrease the flow rate in small decrements, and repeat the above steps.
