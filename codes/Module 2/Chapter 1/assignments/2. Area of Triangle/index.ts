function calculateArea() {
  //function to calculate area
  var point1Element: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("p1")
  );
  var point2Element: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("p2")
  );
  var point3Element: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("p3")
  ); //3 input elements to get points
  var resultElement: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("result")
  ); // <p> to display result

  var [x1, y1] = point1Element.value.split(",").map((x) => parseFloat(x)); // split by , and then convert each point to float
  var [x2, y2] = point2Element.value.split(",").map((x) => parseFloat(x));
  var [x3, y3] = point3Element.value.split(",").map((x) => parseFloat(x));
  if (
    (isNaN(x1) || isNaN(y1) || isNaN(x2), isNaN(y2) || isNaN(x3) || isNaN(y3)) //checks for invalid input
  ) {
    resultElement.innerHTML = "Please enter coordinates correctly in form x,y";
  } else {
    var area: number = Math.abs(
      (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2 //formula
    );
    resultElement.innerHTML = "Area of triangle is " + area + " sq units";
  }
}
