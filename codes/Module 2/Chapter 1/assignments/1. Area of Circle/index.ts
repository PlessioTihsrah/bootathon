function calculateArea() {
  var radiusInput: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("radius")
  ); //get input element that contains user's input
  var result: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("result")
  ); //get <p> for displaying result
  var radius: number = parseFloat(radiusInput.value);
  if (isNaN(radius)) {
    //checks for invalid input
    result.innerHTML = "Please enter valid number";
  } else {
    var area: number = Math.PI * radius * radius; // area calculation
    result.innerHTML = "Area of circle is " + area + " sq units";
  }
}
