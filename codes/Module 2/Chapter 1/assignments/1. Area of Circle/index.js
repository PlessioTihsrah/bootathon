function calculateArea() {
    var radiusInput = (document.getElementById("radius")); //get input element that contains user's input
    var result = (document.getElementById("result")); //get <p> for displaying result
    var radius = parseFloat(radiusInput.value);
    if (isNaN(radius)) {
        //checks for invalid input
        result.innerHTML = "Please enter valid number";
    }
    else {
        var area = Math.PI * radius * radius; // area calculation
        result.innerHTML = "Area of circle is " + area + " sq units";
    }
}
//# sourceMappingURL=index.js.map