# Math Function assignment:

1. Find the area of a circle.
2. To find area of a triangle whose three points are given.
3. Find the value of (x + cos(x)) where x is a number taken as input from the user.
4. Make a scientific calculator with separate functions for addition, subtraction, multiplication, division, sin , cos, tan, square root and power.
