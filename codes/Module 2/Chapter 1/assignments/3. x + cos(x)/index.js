function calculateValue() {
    var xInputElement = (document.getElementById("x")); //gets Input element that contains x
    var resultElement = (document.getElementById("result")); //get p element for result display
    var x = parseFloat(xInputElement.value); //convert to float from string
    if (isNaN(x)) {
        //checks for invalid input
        resultElement.innerHTML = "Invalid Number";
    }
    else {
        var res = x + Math.cos(x);
        resultElement.innerHTML = "x + cos(x) is " + res;
    }
}
//# sourceMappingURL=index.js.map