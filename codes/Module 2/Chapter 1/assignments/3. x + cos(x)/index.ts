function calculateValue() {
  var xInputElement: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("x")
  ); //gets Input element that contains x
  var resultElement: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("result")
  ); //get p element for result display
  var x: number = parseFloat(xInputElement.value); //convert to float from string
  if (isNaN(x)) {
    //checks for invalid input
    resultElement.innerHTML = "Invalid Number";
  } else {
    var res = x + Math.cos(x);
    resultElement.innerHTML = "x + cos(x) is " + res;
  }
}
