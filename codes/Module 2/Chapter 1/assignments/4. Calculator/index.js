function getNumbericValues() {
    //helper function for code reusability
    var number1Element = (document.getElementById("num1")); //gets input element that will have first number entered by user
    var number2Element = (document.getElementById("num2")); //gets input element that will have second number entered by user
    var num1 = parseFloat(number1Element.value); //parse values
    var num2 = parseFloat(number2Element.value);
    return { num1, num2 }; //returns object containing num1 and num2
}
function getTrigonometricData() {
    //helper function for code reusability
    var numberElement = (document.getElementById("num1")); //gets input element that will have first number entered by user
    var radiansRadioElement = (document.getElementById("radians")); //gets input element for radians
    if (radiansRadioElement.checked) {
        // will be true if radio button is on radians
        return parseFloat(numberElement.value); // no need to convert
    }
    else {
        return (parseFloat(numberElement.value) * Math.PI) / 180; // convert degrees to radians and return
    }
}
function setResult(res) {
    // helper function to display result
    var resultElement = document.getElementById("result");
    if (isNaN(res)) {
        //checks for invalid input
        resultElement.value = "Invalid Input Numbers";
    }
    else {
        resultElement.value = res.toFixed(2); // display result upto 2 decimal points
    }
}
function add() {
    //function for addition
    var { num1, num2 } = getNumbericValues();
    setResult(num1 + num2);
}
function subtract() {
    //function for subtraction
    var { num1, num2 } = getNumbericValues();
    setResult(num1 - num2);
}
function multiply() {
    //function for multiplication
    var { num1, num2 } = getNumbericValues();
    setResult(num1 * num2);
}
function divide() {
    // function for division
    var { num1, num2 } = getNumbericValues();
    setResult(num1 / num2);
}
function power() {
    // function for power
    var { num1, num2 } = getNumbericValues();
    setResult(Math.pow(num1, num2));
}
function sqrt() {
    //function for square root
    var { num1 } = getNumbericValues();
    setResult(Math.sqrt(num1));
}
function sin() {
    //function for sin
    var angle = getTrigonometricData();
    setResult(Math.sin(angle));
}
function cos() {
    // function for cosine
    var angle = getTrigonometricData();
    setResult(Math.cos(angle));
}
function tan() {
    // function for tan
    var angle = getTrigonometricData();
    setResult(Math.tan(angle));
}
//# sourceMappingURL=index.js.map