function start() {
  var numElement: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("num")
  );
  var num: number = parseInt(numElement.value);
  var resultEl: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("result")
  );
  if (isNaN(num) || num <= 0) {
    // checks for invalid number
    resultEl.innerHTML = "Invalid Number. Enter Positive Number > 0";
  } else {
    var zero: number = 0; // count of zeros
    var negative: number = 0; //count of negative
    var positive: number = 0; // count of positive
    while (num > 0) {
      var temp: number = parseInt(prompt("Enter Number")); // get input from user
      if (temp === 0) {
        // zero
        zero++;
      } else if (temp > 0) {
        // positive
        positive++;
      } else if (temp < 0) {
        // negative, this condition eliminates invalid numbers being counted as negative
        negative++;
      }
      num--; //decrement counter
    }
    resultEl.innerHTML = //add result
      "Zeros: " +
      zero +
      "<br>Positive Numbers: " +
      positive +
      "<br>Negative Numbers: " +
      negative;
  }
}
