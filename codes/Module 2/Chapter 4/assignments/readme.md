# Module 2: Chapter 4 Looping Assignments :-

1. Assignment 1 :
   Accept the number of numbers that user will enter in a text box. Accept those many numbers from the user using prompt tag (they can be +ve numbers, -ve numbers or 0). In the end, display in the p tag, No. of positive numbers entered, No. of negative numbers entered, No. of times 0 was entered.
   Hint: keep separate counting variables for +ve, -ve and 0 and print the total for each. Use if else and while loop.

2. Assignment 2 :
   Write a program to print Armstrong numbers between 100 and 999 in p tag. A number is an Armstrong number if sum of each digit raised to no. of digits of the number is equal to the number itself.
   For example: 153=(1*1*1)+(5*5*5)+(3*3*3).

3. Assignment 3 :
   Write a program to create dynamic table generation for multiplication table of any number and table should be from 1 to entered number. For example :
   When user will enter number as 6 then table will start from 6 _ 1 = 6 and it will end at 6 _ 6 = 36.
