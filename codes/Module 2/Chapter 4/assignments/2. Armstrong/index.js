function checkArmstrong(num) {
    // helper function to check if number is armstrong
    var copy = num.toString(); // make a copy of number as string
    for (var i = 0; i < copy.length; i++) {
        //iterate over each digit
        var digit = parseInt(copy[i]); //parse int
        num -= Math.pow(digit, copy.length); // subtract digit to power n from number
    }
    return num === 0; // return true if num === sum of digits raised to power no. of digits
}
var resultElement = (document.getElementById("result")); // get result paragraph
var resultString = ""; //will contain result
for (var i = 100; i <= 999; i++) {
    //check armstrong between 100 and 999
    if (checkArmstrong(i)) {
        //checks if i is armstrong
        resultString += i + " "; // add i to result
    }
}
resultElement.innerHTML = resultString; //append result
//# sourceMappingURL=index.js.map