function checkArmstrong(num: number) {
  // helper function to check if number is armstrong
  var copy: string = num.toString(); // make a copy of number as string
  for (var i: number = 0; i < copy.length; i++) {
    //iterate over each digit
    var digit: number = parseInt(copy[i]); //parse int
    num -= Math.pow(digit, copy.length); // subtract digit to power n from number
  }
  return num === 0; // return true if num === sum of digits raised to power no. of digits
}

var resultElement: HTMLParagraphElement = <HTMLParagraphElement>(
  document.getElementById("result")
); // get result paragraph
var resultString: string = ""; //will contain result

for (var i: number = 100; i <= 999; i++) {
  //check armstrong between 100 and 999
  if (checkArmstrong(i)) {
    //checks if i is armstrong
    resultString += i + " "; // add i to result
  }
}

resultElement.innerHTML = resultString; //append result
