function printTable() {
  var numElement: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("number")
  );
  var num = parseInt(numElement.value); //get number to print table
  var errorParagraph: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("error")
  ); // paragraph to display error incase of invalid input
  var tableElement: HTMLTableElement = <HTMLTableElement>(
    document.getElementById("table")
  ); // get table element
  tableElement.innerHTML = ""; // reset all content in table
  if (isNaN(num) || num < 1) {
    // checks invalid input
    errorParagraph.innerHTML = "Invalid Number";
  } else {
    errorParagraph.innerHTML = ""; // reset error
    for (var i = 1; i <= num; i++) {
      //run till 1 to num
      var row: HTMLTableRowElement = tableElement.insertRow(); //add a row
      var tcell: HTMLTableCellElement = row.insertCell(); // insert a cell
      var text: HTMLParagraphElement = document.createElement("p"); // create p element
      text.innerHTML = num.toString(); // add num to p
      tcell.appendChild(text); // append p to table cell and repeat same procedure
      tcell = row.insertCell();
      text = document.createElement("p");
      text.innerHTML = "*";
      tcell.appendChild(text);
      tcell = row.insertCell();
      text = document.createElement("p");
      text.innerHTML = i.toString();
      tcell.appendChild(text);
      tcell = row.insertCell();
      text = document.createElement("p");
      text.innerHTML = "=";
      tcell.appendChild(text);
      tcell = row.insertCell();
      text = document.createElement("p");
      text.innerHTML = (i * num).toString();
      tcell.appendChild(text);
    }
  }
}
