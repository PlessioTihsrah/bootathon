function checkValidTriangle(side1, side2, side3) {
    //checks if a triangle can be formed
    if (side1 < side2 + side3 && side2 < side1 + side3 && side3 < side1 + side2) {
        return true; //triangle can be formed
    }
    else {
        return false; //triangle cannot be formed as sum of 2 sides is not less than third side
    }
}
function checkRightAngled(side1, side2, side3) {
    //checks if triangle is right angled by pythagoras theorem
    if (Math.pow(side1, 2) === Math.pow(side2, 2) + Math.pow(side3, 2)) {
        return true;
    }
    else if (Math.pow(side2, 2) === Math.pow(side1, 2) + Math.pow(side3, 2)) {
        return true;
    }
    else if (Math.pow(side3, 2) === Math.pow(side1, 2) + Math.pow(side2, 2)) {
        return true;
    }
    else {
        return false; // not right angled
    }
}
function tellType() {
    var side1Element = (document.getElementById("side1"));
    var side2Element = (document.getElementById("side2"));
    var side3Element = (document.getElementById("side3")); //gets input element for 3 sides
    var side1 = parseFloat(side1Element.value);
    var side2 = parseFloat(side2Element.value);
    var side3 = parseFloat(side3Element.value);
    //parses sides
    var resultElement = (document.getElementById("result")); //gets p tag for displaying result
    if (isNaN(side1) || isNaN(side2) || isNaN(side3)) {
        //checks for invalid input
        resultElement.innerHTML = "Invalid Sides";
    }
    else if (!checkValidTriangle(side1, side2, side3)) {
        //check whether triangle can be formed
        resultElement.innerHTML =
            "Invalid Triangle. Sum of 2 Sides should be greater than third Side";
    }
    else if (side1 === side2 && side2 === side3) {
        //all sides equal
        resultElement.innerHTML = "It is equilateral triangle";
    }
    else if (side1 === side2 || side1 === side3 || side2 === side3) {
        //2 sides equal
        if (checkRightAngled(side1, side2, side3)) {
            //check for right angled
            resultElement.innerHTML = "It is right angled isosceles triangle";
        }
        else {
            resultElement.innerHTML = "It is isosceles triangle";
        }
    }
    else {
        // no sides equal
        if (checkRightAngled(side1, side2, side3)) {
            //check for right angle
            resultElement.innerHTML = "It is scalene right angled triangle";
        }
        else {
            resultElement.innerHTML = "It is scalene triangle";
        }
    }
}
//# sourceMappingURL=index.js.map