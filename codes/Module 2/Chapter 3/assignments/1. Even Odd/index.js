function checkEvenOdd() {
    var numElement = document.getElementById("num");
    var resultElement = document.getElementById("result");
    var num = parseInt(numElement.value); //parse value of input
    if (isNaN(num)) {
        //checks for invalid input
        resultElement.innerHTML = "Invalid Number";
    }
    else if (num % 2 == 0) {
        //ends with multiple of 2
        resultElement.innerHTML = num + " is even";
    }
    else {
        resultElement.innerHTML = num + " is odd";
    }
}
//# sourceMappingURL=index.js.map