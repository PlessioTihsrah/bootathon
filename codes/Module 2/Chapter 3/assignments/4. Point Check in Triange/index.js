function calculateArea(//helper function to calculate area
x1, y1, x2, y2, x3, y3) {
    var area = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
    return area;
}
function parsePoint(inputElement) {
    //gets points and returns [p1, p2]
    return inputElement.value.split(",").map((i) => parseFloat(i));
}
function checkPoint() {
    var [x1, y1] = parsePoint(document.getElementById("p1"));
    var [x2, y2] = parsePoint(document.getElementById("p2"));
    var [x3, y3] = parsePoint(document.getElementById("p3"));
    var [x4, y4] = parsePoint(document.getElementById("p4"));
    //stored all 4 points in x,y
    var resultElement = (document.getElementById("result")); // get p tag for displaying result
    if (isNaN(x1) ||
        isNaN(x2) ||
        isNaN(x3) ||
        isNaN(x4) ||
        isNaN(y1) ||
        isNaN(y2) ||
        isNaN(y3) ||
        isNaN(y4)) {
        // check for invalid input
        resultElement.innerHTML = "Invalid Points";
    }
    else {
        //inputs are ok
        var areaOfTriange = calculateArea(x1, y1, x2, y2, x3, y3); // area of triangle
        var a1 = calculateArea(x2, y2, x3, y3, x4, y4); //sub area 1
        var a2 = calculateArea(x1, y1, x3, y3, x4, y4); // sub area 2
        var a3 = calculateArea(x1, y1, x2, y2, x4, y4); // sub area 3
        if (Math.abs(areaOfTriange - (a1 + a2 + a3)) < 0.001) {
            // sum of whole - sum of all sub areas should be approximately equal to 0
            resultElement.innerHTML = "Point lies inside triange";
        }
        else {
            // point outside
            resultElement.innerHTML = "Point lies outside triange";
        }
    }
}
//# sourceMappingURL=index.js.map