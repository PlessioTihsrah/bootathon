function find() {
    var inputElement = (document.getElementById("complex")); // get input element
    var complexString = inputElement.value; //get string
    var offset = ""; // used to check if there is + or - sign in start of string
    if (complexString[0] === "+" || complexString[0] === "-") {
        offset = complexString[0]; // set offset to start sign
        complexString = complexString.substring(1); // remove sign from start of string
    }
    var negativeImaginary = complexString.includes("-"); // complex part is negative
    var resultElement = (document.getElementById("result")); //get p tag for result
    var result; // string for result
    if (negativeImaginary) {
        var [real, imaginary] = complexString.split("-"); // split into real and imaginary part
        result =
            "Real: " +
                offset +
                parseInt(real) +
                " Imaginary: -" +
                (parseInt(imaginary) || 0).toString(); //put real and imaginary into result. If imaginary is not present, it will be zero
    }
    else {
        var [real, imaginary] = complexString.split("+"); // split into real and imaginary
        result =
            "Real: " +
                offset +
                parseInt(real) +
                " Imaginary: +" +
                (parseInt(imaginary) || 0).toString();
    }
    resultElement.innerHTML = result; //append result
}
//# sourceMappingURL=index.js.map