function find() {
  var inputElement: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("complex")
  ); // get input element
  var complexString: string = inputElement.value; //get string
  var offset: string = ""; // used to check if there is + or - sign in start of string
  if (complexString[0] === "+" || complexString[0] === "-") {
    offset = complexString[0]; // set offset to start sign
    complexString = complexString.substring(1); // remove sign from start of string
  }
  var negativeImaginary: boolean = complexString.includes("-"); // complex part is negative
  var resultElement: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("result")
  ); //get p tag for result
  var result: string; // string for result
  if (negativeImaginary) {
    var [real, imaginary] = complexString.split("-"); // split into real and imaginary part
    result =
      "Real: " +
      offset +
      parseInt(real) +
      " Imaginary: -" +
      (parseInt(imaginary) || 0).toString(); //put real and imaginary into result. If imaginary is not present, it will be zero
  } else {
    var [real, imaginary] = complexString.split("+"); // split into real and imaginary
    result =
      "Real: " +
      offset +
      parseInt(real) +
      " Imaginary: +" +
      (parseInt(imaginary) || 0).toString();
  }
  resultElement.innerHTML = result; //append result
}
