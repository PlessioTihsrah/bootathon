function getResult() {
    var inputElement = (document.getElementById("text")); //gets input element
    var inputString = inputElement.value.toUpperCase(); //convert input string to uppercase
    var resultElement = (document.getElementById("result")); // gets p element for displaying result
    resultElement.innerHTML = inputString.substring(4); //substring from index 4 to length
    var position = inputString.indexOf("E"); //returns first index of E or -1
    resultElement.innerHTML += "<br>Index of E: " + position; //append index to result
}
//# sourceMappingURL=index.js.map