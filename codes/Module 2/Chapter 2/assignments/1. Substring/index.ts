function getResult() {
  var inputElement: HTMLInputElement = <HTMLInputElement>(
    document.getElementById("text")
  ); //gets input element
  var inputString: string = inputElement.value.toUpperCase(); //convert input string to uppercase
  var resultElement: HTMLParagraphElement = <HTMLParagraphElement>(
    document.getElementById("result")
  ); // gets p element for displaying result
  resultElement.innerHTML = inputString.substring(4); //substring from index 4 to length
  var position: number = inputString.indexOf("E"); //returns first index of E or -1
  resultElement.innerHTML += "<br>Index of E: " + position; //append index to result
}
