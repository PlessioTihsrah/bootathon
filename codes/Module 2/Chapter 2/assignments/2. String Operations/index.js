function performOperations() {
    var inputElement = (document.getElementById("inputString"));
    var inputString = inputElement.value;
    var resultElement = (document.getElementById("result"));
    if (inputString.length === 0) {
        //checks for empty input
        resultElement.innerHTML = "Please Enter any String";
    }
    else {
        resultElement.innerHTML = "Uppercase: " + inputString.toUpperCase(); //uppercase
        resultElement.innerHTML += "<br>Lowercase: " + inputString.toLowerCase(); //lowercase
        resultElement.innerHTML +=
            "<br>Splitted at spaces: " + inputString.split(" "); //split by space
    }
}
//# sourceMappingURL=index.js.map